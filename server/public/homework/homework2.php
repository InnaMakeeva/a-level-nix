<?php
$name = 'Инна';
echo $name . '\n';
print $name . '\n';
//print_r($name);

$age = 34;
echo '\n' . $age . '\n';
print $age . '\n';
//print_r($age);

$pi = 3.14;
echo '\n' . $pi . '\n';
print $pi . '\n';
//print_r($pi);

$array1 = ['alex', 'vova', 'tolya'];
echo '\n' . $array1 . '\n';
print $array1 . '\n';
//print_r($array1);

$array2 = ['alex', 'vova', 'tolya', ['kostya', 'olya']];
echo '\n' . $array2 . '\n';
print $array2 . '\n';
//print_r($array2);

$array3 = ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', 'mila']]];
echo '\n' . $array3 . '\n';
print $array3 . '\n';
//print_r($array3);

$array4 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', 'mila']];
echo '\n' . $array4 . '\n';
print $array4 . '\n';
//print_r($array4);
