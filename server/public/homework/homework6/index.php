<?php

/**
 * @param array $arr
 * @return boolean
 */
function dd(array $arr): bool
{
    // phpcs:disable
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    // phpcs:enable

    return true;
}

//1. Вам нужно создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand). Далее, вычислить произведение
// тех элементов, которые больше нуля и у которых четные индексы. После вывести на экран элементы, которые больше нуля
// и у которых нечетный индекс.
/**
 * @param integer $number
 * @return array
 */
function calcNum($number): array
{
    $arr = [];
    $result = [];
    $mult = 1;
    for ($i = 0; $i < $number; $i++) {
        $num = rand(1, 5);
        $arr[] = $num;
    }
    foreach ($arr as $key => $value) {
        if (($key % 2 == 0) && $value > 0) {
            $mult *= $value;
        }

        if (!($key % 2 == 0) && $value > 0) {
            $result[] = $value;
        }
    }
    return $result;
}
dd(calcNum(100));

//2. Даны два числа. Найти их сумму и произведение. Даны два числа. Найдите сумму их квадратов.
/**
 * @param integer $x
 * @param integer $y
 * @return string
 */
function calcSum($x = 3, $y = 5): string
{
    $sum = $x + $y;
    $mult = $x * $y;
    $deg = $x ** 2 + $y ** 2;
    return 'Сумма чисел ' . $x . ' и ' . $y . ' равна ' . $sum . '<br>' .
        'Произведение чисел ' . $x . ' и ' . $y . ' равна ' . $mult . '<br>' .
        'Сумма квадратов чисел ' . $x . ' и ' . $y . ' равна ' . $deg . '<br>';
}
echo calcSum(2, 2);
echo calcSum();

//3. Даны три числа. Найдите их среднее арифметическое.

/**
 * @param integer $x
 * @param integer $y
 * @param integer $z
 * @return string
 */
function calcAverage($x = 3, $y = 5, $z = 1): string
{
    $average = ($x + $y + $z) / 3;
    return 'Среднее арифметическое ' .  $x . ' , ' . $y . ' , ' . $z . ' равно ' . $average;
}
echo calcAverage(1, 2, 3);

//4. Дано число. Увеличьте его на 30%, на 120%.

/**
 * @param integer $x
 * @return string
 */
function calcPercent($x = 30): string
{
    return '30% от числа ' . $x . ' составляет ' . $x * 0.3 . '<br>' .
        '120% от числа ' . $x . ' составляет ' . $x * 1.2 . '<br>';
}

echo calcPercent(5);

//5. Пользователь выбирает из выпадающего списка страну
// (Турция, Египет или Италия), вводит количество дней для отдыха
// и указывает, есть ли у него скидка (чекбокс). Вывести стоимость отдыха,
// которая вычисляется как произведение   количества дней на 400.
// Далее это число увеличивается на 10%, если выбран Египет, и на 12%, если выбрана Италия.
// И далее это число уменьшается на 5%, если указана скидка.
// phpcs:disable
function showMessage()
{
    // phpcs:enable
    include 'formTravel.html';
    $price = 0;
    if ($_POST['country'] == 'Turkey') {
        if ($_POST['yes'] == 'on') {
            $price = ($_POST['days'] * 400) - (($_POST['days'] * 400) * 0.05);
        } else {
            $price = $_POST['days'] * 400;
        }
    } elseif ($_POST['country'] == 'Egypt') {
        if ($_POST['yes'] == 'on') {
            $price = (($_POST['days'] * 400) + ($_POST['days'] * 400) * 0.1) -
                (((($_POST['days'] * 400) + (($_POST['days'] * 400) * 0.1))) * 0.05);
        } else {
            $price = ($_POST['days'] * 400) + (($_POST['days'] * 400) * 0.1);
        }
    } elseif ($_POST['country'] == 'Italy') {
        if ($_POST['yes'] == 'on') {
            $price = (($_POST['days'] * 400) + ($_POST['days'] * 400) * 0.12) -
                (((($_POST['days'] * 400) + (($_POST['days'] * 400) * 0.12))) * 0.05);
        } else {
            $price = ($_POST['days'] * 400) + (($_POST['days'] * 400) * 0.12);
        }
    }
    return 'Ваше путешествие будет стоить $price';
}
echo showMessage();

//6. Пользователь вводит свой имя, пароль, email. Если вся информация указана, то показать эти данные после фразы
// 'Регистрация прошла успешно', иначе сообщить какое из полей оказалось не заполненным.
// phpcs:disable
function register()
{
    // phpcs:enable
    include 'formRegistration.html';
    if (empty($_POST['name']) && empty($_POST['password']) && empty($_POST['email'])) {
        echo 'Введите данные!';
    } elseif (!empty($_POST['name']) && !empty($_POST['password']) && !empty($_POST['email'])) {
        echo 'Регистрация прошла успешно';
    } elseif (empty($_POST['name'])) {
        echo 'Вы не ввели имя';
    } elseif (empty($_POST['password'])) {
        echo 'Вы не ввели пароль';
    } elseif (empty($_POST['email'])) {
        echo 'Вы не ввели email';
    }
}
register();

//7. Выведите на экран n раз фразу 'Silence is golden'. Число n вводит пользователь на форме. Если n некорректно, вывести фразу 'Bad n'.
// phpcs:disable
function showMessage()
{
    // phpcs:enable
    include 'formNumber.html';
    $n = $_POST['number'];
    if (empty($n)) {
        echo 'Введите число!';
    } elseif ($n > 0) {
        for ($i = 1; $i <= $n; $i++) {
            echo 'Silence is golden' . '<br>';
        }
    } else {
        echo 'Bad n';
    }
}
showMessage();

//8. Заполнить массив длины n нулями и единицами, при этом данные значения чередуются, начиная с нуля.

/**
 * @param integer $n
 * @return array
 */
function fillArray($n): array
{
    $arr = [];
    $a = 0;
    $b = 1;
    for ($i = 0; $i < $n; $i++) {
        if ($i % 2 == 0) {
            $arr[] = $a;
        } else {
            $arr[] = $b;
        }
    }
    return $arr;
}
dd(fillArray(24));


//9. Определите, есть ли в массиве повторяющиеся элементы.

/**
 * @param integer $x
 * @param integer $y
 * @param integer $z
 * @param integer $a
 * @return string
 */
function findRepetitions($x = 9, $y = 0, $z = 2, $a = 3): string
{
    $arr = [$x, $y, $z, $a];
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }
    for ($i = 0; $i < $count; $i++) {
        for ($j = $i + 1; $j <= $count; $j++) {
            if ($arr[$i] === $arr[$j]) {
                $arrNew[] = $arr[$i];
                break;
            }
        }
    }
    if (empty($arrNew)) {
        return 'Повторяющихся элементов в массиве нет';
    } else {
        return 'Повторяющиеся элементы в массиве есть';
    }
}
echo findRepetitions(5, 5, 5);
echo findRepetitions();

//10. Найти минимальное и максимальное среди 3 чисел

/**
 * @param integer $x
 * @param integer $y
 * @param integer $z
 * @return string
 */
function findMinMax($x = 3, $y = 1, $z = 8): string
{
    $arr = [$x, $y, $z];
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }
    foreach ($arr as $value) {
        for ($j = 0; $j < $count; $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                $num = $arr[$j + 1];
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $num;
            }
        }
    }
    if ($arr[0] == $arr[$count]) {
        return $arr[0] . ' = ' . $arr[$count] . '<br>';
    } else {
        return $arr[0] . '- наименьшее число, ' . $arr[$count] . '- наибольшее число <br>';
    }
}
echo findMinMax(5, 5, 5);
echo findMinMax();

//11. Найти площадь

/**
 * @param integer $a
 * @param integer $b
 * @return string
 */
function calcArea($a = 10, $b = 4): string
{
    return 'Площадь фигуры со сторонами ' . $a . ' и ' . $b . ' составляет ' .
        $a * $b . '<br>';
}
echo calcArea(5);

//12. Теорема Пифагора

/**
 * @param integer $leg1
 * @param integer $leg2
 * @return string
 */
function calcHypotenuse($leg1 = 3, $leg2 = 4): string
{
    $hypotenuse = sqrt($leg1 ** 2 + $leg2 ** 2);
    return 'Гипотенуза треугольника с катетами ' . $leg1 . ' и ' . $leg2 . ' равна ' . $hypotenuse;
}
echo calcHypotenuse();

//13. Найти периметр

/**
 * @param integer $a
 * @param integer $b
 * @return string
 */
function calcPerimeter($a = 10, $b = 4): string
{
    return 'Периметр фигуры со сторонами ' . $a . ' и ' . $b . ' составляет ' .
        ($a + $b) * 2 . '<br>';
}
echo calcPerimeter(5, 3);

//14. Найти дискриминант

/**
 * @param integer $a
 * @param integer $b
 * @param integer $c
 * @return string
 */
function calcDiscriminant($a = 1, $b = 6, $c = 9): string
{
    $discriminant = $b ** 2 - 4 * $a * $c;
    return 'В уравнении axx + bx + c = 0 дискриминант равен ' . $discriminant . '<br>';
}
echo calcDiscriminant(3, 4, 2);
echo calcDiscriminant(1, -4, -5);

//15. Создать только четные числа до 100

/**
 * @param integer $numb
 * @return array
 */
function createEven($numb): array
{
    $num = [];
    for ($i = 2; $i <= $numb; $i += 2) {
        $num[] = $i;
    }
    return $num;
}
dd(createEven(100));


//16. Создать не четные числа до 100

/**
 * @param integer $numb
 * @return array
 */
function createOdd($numb): array
{
    $num = [];
    for ($i = 1; $i <= $numb; $i += 2) {
        $num[] = $i;
    }
    return $num;
}
createOdd(100);

//17. Создать функцию по нахождению числа в степени

/**
 * @param integer $num
 * @param integer $degree
 * @return integer
 */
function calcDegree($num = 5, $degree = 3): int
{
    return $num ** $degree;
}
echo calcDegree(2, 3);


//18. написать функцию сортировки. Функция принимает массив случайных чисел и
// сортирует их по порядку. По дефолту функция сортирует в порядке возрастания.
// Но если передать в сторой параметр то функция будет сортировать по убыванию.

/**
 * @param array  $arr
 * @param string $sort
 * @return array
 */
function sortArray($arr, $sort = 'adc'): array
{
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }
    if ($sort == 'adc') {
        foreach ($arr as $value) {
            for ($j = 0; $j < $count; $j++) {
                if ($arr[$j] > $arr[$j + 1]) {
                    $num = $arr[$j + 1];
                    $arr[$j + 1] = $arr[$j];
                    $arr[$j] = $num;
                }
            }
        }
    } else {
        foreach ($arr as $value) {
            for ($j = 0; $j < $count; $j++) {
                if ($arr[$j] < $arr[$j + 1]) {
                    $num = $arr[$j + 1];
                    $arr[$j + 1] = $arr[$j];
                    $arr[$j] = $num;
                }
            }
        }
    }
    return $arr;
}
dd(sortArray([2, 11, 34, 3, 5, 113], 1));


//19. написать функцию поиска в массиве. функция будет принимать два параметра. Первый массив, второй поисковое число. search(arr, find)

/**
 * @param array   $arr
 * @param integer $find
 * @return string
 */
function searchElem($arr, $find): string
{
    foreach ($arr as $value) {
        if ($value == $find) {
            echo 'Элемент ' . $find . ' найден!';
            break;
        }
    }
    return true;
}
searchElem([2, 11, 34, 3, 5, 113], 5);
