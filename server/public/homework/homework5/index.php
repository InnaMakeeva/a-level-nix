<?php
// phpcs:disable
echo '<h2>Угадай число</h2>';
// phpcs:enable
require 'form1.html';
$a = rand(5, 8);
$b = $_POST['id'];

if ($b < 5 && $b != null) {
    echo 'Число маленькое';
} elseif ($b > 8 && $b != null) {
    echo 'Число большое';
} elseif ($b == $a && $b != null) {
    echo "Вы угадали! <a href='index.php'>Играть еще</a>";
} elseif ($b != $a && $b != null) {
    echo "<a href='index.php'>Попробуй еще</a>";
}
