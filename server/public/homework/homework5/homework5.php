<?php

//1) Написать программу, которая выводит простые числа, т.е. делящиеся без
//остатка только на себя и на 1.

// while

$n = rand(1, 100);
$no_prime = 0;
$prime = 0;
$i = 2;
$j = 2;
while ($i <= $n) {
    while ($j < $i) {
        if ($i % $j == 0) {
            $no_prime = $i;
            break;
        }
        $j++;
    }
    if ($i != $no_prime) {
        $prime = $i;
        echo $prime . ' - простое число' . '<br>';
    }
    if ($j == $i) {
        $j = 2;}
    $i++;
}

//for

$n = rand(1, 100);
$no_prime = 0;
$prime = 0;
for ($i = 2; $i <= $n; $i++) {
    for ($j = 2; $j < $i; $j++) {
        if ($i % $j == 0) {
            $no_prime = $i;
            break;
        }
    }
    if ($i != $no_prime) {
        $prime = $i;
        echo $prime . ' - простое число' . '<br>';
    }
}

//2) Сгенерируйте 100 раз новое число и выведите на экран количество четных чисел из этих 100.

// while
$counter = 0;
$i = 1;
while ($i <= 100) {
    $n = rand(1, 10);
    if ($n % 2 == 0) {
        $counter++;
    }
    $i++;
}
echo 'Четных чисел ' . $counter . ' из 100';

//do while
$counter = 0;
$i = 0;
do {
    $n = rand(1, 10);
    if ($n % 2 == 0) {
        $counter++;
    }
    $i++;
} while ($i < 100);
echo 'Четных чисел ' . $counter . ' из 100';


// for
$counter = 0;
for ($i = 1; $i <= 100; $i++) {
    $n = rand(1, 10);
    if ($n % 2 == 0) {
        $counter++;
    }
}

echo 'Четных чисел ' . $counter . ' из 100';

//foreach

$arr = [];
$counter = 0;
for ($i = 1; $i <= 100; $i++) {
    $n = rand(1, 10);
    $arr[] = $n;
}
foreach ($arr as $value) {
    if ($value % 2 == 0) {
        $counter++;
    }
}
echo 'Четных чисел ' . $counter . ' из 100';

//3) Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз сгенерировались эти числа (1, 2, 3, 4 и 5).

//while

$counter1 = 0;
$counter2 = 0;
$counter3 = 0;
$counter4 = 0;
$counter5 = 0;
$i = 1;
while ($i <= 100) {
    $n = rand(1, 5);
    if ($n == 1) {
        $counter1++;
    } elseif ($n == 2) {
        $counter2++;
    } elseif ($n == 3) {
        $counter3++;
    } elseif ($n == 4) {
        $counter4++;
    } else {
        $counter5++;
    }
    $i++;
}
echo $counter1 . '<br>' . $counter2 . '<br>' .  $counter3 . '<br>' . $counter4 . '<br>' . $counter5;

//do while

$counter1 = 0;
$counter2 = 0;
$counter3 = 0;
$counter4 = 0;
$counter5 = 0;
$i = 0;
do {
    $n = rand(1, 5);
    if ($n == 1) {
        $counter1++;
    } elseif ($n == 2) {
        $counter2++;
    } elseif ($n == 3) {
        $counter3++;
    } elseif ($n == 4) {
        $counter4++;
    } else {
        $counter5++;
    }
    $i++;
} while ($i < 100);
echo $counter1 . '<br>' . $counter2 . '<br>' .  $counter3 . '<br>' . $counter4 . '<br>' . $counter5;

//for

$counter1 = 0;
$counter2 = 0;
$counter3 = 0;
$counter4 = 0;
$counter5 = 0;
for ($i = 1; $i <= 100; $i++) {
    $n = rand(1, 5);
    if ($n == 1) {
        $counter1++;
    } elseif ($n == 2) {
        $counter2++;
    } elseif ($n == 3) {
        $counter3++;
    } elseif ($n == 4) {
        $counter4++;
    } else {
        $counter5++;
    }
}
echo $counter1 . '<br>' . $counter2 . '<br>' .  $counter3 . '<br>' . $counter4 . '<br>' . $counter5;

//foreach

$counter1 = 0;
$counter2 = 0;
$counter3 = 0;
$counter4 = 0;
$counter5 = 0;
$arr = [];
$counter = 0;
for ($i = 1; $i <= 100; $i++) {
    $n = rand(1, 5);
    $arr[] = $n;
}
foreach ($arr as $value) {
    if ($value == 1) {
        $counter1++;
    } elseif ($value == 2) {
        $counter2++;
    } elseif ($value == 3) {
        $counter3++;
    } elseif ($value == 4) {
        $counter4++;
    } else {
        $counter5++;
    }
}
echo $counter1 . '<br>' . $counter2 . '<br>' .  $counter3 . '<br>' . $counter4 . '<br>' . $counter5;



//4) Используя условия и циклы сделать таблицу в 5 колонок и 3 строки (5x3), отметить разными цветами часть ячеек.

//while

echo "<table border='2'";
$x = 0;
$y = 0;
while ($x < 3) {
    echo '<tr>';
    if ($y == 5) {
        $y = 0;}
    while ($y < 5) {
        $color1 = rand(1, 256);
        $color2 = rand(1, 256);
        $color3 = rand(1, 256);
        // phpcs:disable
        echo "<td style=background-color:rgb($color1,$color2,$color3);>&nbps</td>";
        // phpcs:enable
        $y++;
    }
    echo '</tr>';
    $x++;
}
echo '</table>';

//do while
echo "<table border='2'";
$x = 0;
$y = 0;
do {
    echo '<tr>';
    if ($y == 5) {
        $y = 0; }
    do {
        $color1 = rand(1, 256);
        $color2 = rand(1, 256);
        $color3 = rand(1, 256);
        // phpcs:disable
        echo "<td style=background-color:rgb($color1,$color2,$color3);>&nbps</td>";
        // phpcs:enable
        $y++;
    } while ($y < 5);
    echo '</tr>';
    $x++;
} while ($x < 3);
echo '</table>';

//for
echo "<table border='2'";
for ($x = 0; $x < 3; $x++) {
    echo '<tr>';
    for ($y = 0; $y < 5; $y++) {
        $color1 = rand(1, 256);
        $color2 = rand(1, 256);
        $color3 = rand(1, 256);
        // phpcs:disable
        echo "<td style=background-color:rgb($color1,$color2,$color3);>&nbps</td>";
        // phpcs:enable
    }
    echo '</tr>';
}
echo '</table>';

// 1. В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую пору года попадает этот месяц (зима, лето, весна, осень).

$month = rand(1, 12);
if ($month <= 2 || $month == 12) {
    echo 'Месяц ' . $month . '. ' . 'Сейчас зима.';
} elseif ($month > 2 && $month <= 5) {
    echo 'Месяц ' . $month . '. ' . 'Сейчас весна.';
} elseif ($month > 5 && $month <= 8) {
    echo 'Месяц ' . $month . '. ' . 'Сейчас лето.';
} elseif ($month > 8 && $month <= 11) {
    echo 'Месяц ' . $month . '. ' . 'Сейчас осень.';
}

// 2. Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым символом этой строки является буква 'a'. Если это так - выведите 'да', в противном случае выведите 'нет'.

$str = 'abcde';
echo $str[0] == 'a' ? 'да' : 'нет';

// 3. Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки является цифра 1, 2 или 3. Если это так - выведите 'да', в противном случае выведите 'нет'.

$str = '12345';
echo ($str[0] == '1' || $str[0] == '2' || $str[0] == '3') ? 'да' : 'нет';

// 4. Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при test, равном true, false. Напишите два варианта скрипта - тернарка и if else.

$test = '';
echo ($test) ? 'Верно' : 'Неверно';

$test = ' ';
echo ($test) ? 'Верно' : 'Неверно';

$test = '.';
if ($test) {
    echo 'Верно';
} else {
    echo 'Неверно';
}

$test = '0';
if ($test) {
    echo 'Верно';
} else {
    echo 'Неверно';
}

// 5.Дано Два массива рус и англ ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
//Если переменная lang = ru вывести массив на русском языке, а если en то вывести на английском языке. Сделат через if else и через тернарку.

$lang = 'ru';
if ($lang == 'ru') {
    print(['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']);
} else {
    print(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']);
}

$lang = '';
print $lang == 'ru' ? ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'] : ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

// 6. В переменной cloсk лежит число от 0 до 59 – это минуты. Определите в какую четверть часа попадает это число (в первую, вторую, третью или четвертую). тернарка и if else.

$clock = rand(0, 59);
if ($clock >= 1 && $clock <= 15) {
    echo $clock . ' мин. - это первая четверть часа.';
} elseif ($clock >= 16 && $clock <= 30) {
    echo $clock . ' мин. - это вторая четверть часа.';
} elseif ($clock >= 31 && $clock <= 45) {
    echo $clock . ' мин. - это третья четверть часа.';
} elseif (($clock >= 46 && $clock <= 59) || $clock == 0) {
    echo $clock . ' мин. - это четвертая четверть часа.';
}


$clock = rand(0, 59);
echo $clock >= 1 && $clock <= 15 ? $clock . ' мин. - это первая четверть часа.' :
    ($clock >= 16 && $clock <= 30 ? $clock . ' мин. - это вторая четверть часа.' :
        ($clock >= 31 && $clock <= 45 ? $clock . ' мин. - это третья четверть часа.' :
            ($clock >= 46 && $clock <= 59 || $clock == 0 ? $clock . '  мин. - это четвертая четверть часа.' :
                'попробуй еще раз')));


// 1. Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//Развернуть этот массив в обратном направлении

//while

$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$i = 0;
$index = 0;
$reverseName = [];
while (isset($names[$i])) {
    $index = $i;
    $i++;
}

while ($index >= 0) {
    $reverseName[] = $names[$index];
    $index--;
}
echo '<pre>';
print($reverseName);
echo '</pre>';

//do while

$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$i = 0;
$reverseName = [];
while (isset($names[$i])) {
    $index = $i;
    $i++;
}

do {
    $reverseName[] = $names[$index];
    $index--;
} while ($index >= 0);
echo '<pre>';
print($reverseName);
echo '</pre>';

//for

$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$index = 0;
$reverseName = [];
for ($i = 0; $i >= 0; $i++) {
    if (isset($names[$i])) {
        $index = $i;
    } else {
        break;
    }
}

for ($i = $index; $i >= 0; $i--) {
    $reverseName[] = $names[$i];
}
echo '<pre>';
print($reverseName);
echo '</pre>';

//foreach

$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$index = 0;
$reverseName = [];
foreach ($names as $key => $value) {
    if (isset($names[$key])) {
        $index = $key;
    }
}
foreach ($names as $key => $value) {
    $reverseName[] = $names[$index];
    $index--;
}
echo '<pre>';
print($reverseName);
echo '</pre>';

//2. Дан массив
//[44, 12, 11, 7, 1, 99, 43, 5, 69]
//Развернуть этот массив в обратном направлении

//while

$num = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$i = 0;
$index = 0;
$reverseNum = [];
while (isset($num[$i])) {
    $index = $i;
    $i++;
}
while ($index >= 0) {
    $reverseNum[] = $num[$index];
    $index--;
}
echo '<pre>';
print($reverseNum);
echo '</pre>';

//do while

$num = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$i = 0;
$reverseNum = [];
while (isset($num[$i])) {
    $index = $i;
    $i++;
}
do {
    $reverseNum[] = $num[$index];
    $index--;
} while ($index >= 0);
echo '<pre>';
print($reverseNum);
echo '</pre>';

//for

$num = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$index = 0;
$reverseNum = [];
for ($i = 0; $i >= 0; $i++) {
    if (isset($num[$i])) {
        $index = $i;
    } else {
        break;
    }
}
for ($i = $index; $i >= 0; $i--) {
    $reverseNum[] = $num[$i];
}
echo '<pre>';
print($reverseNum);
echo '</pre>';

//foreach

$num = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$index = 0;
$reverseNum = [];
foreach ($num as $key => $value) {
    if (isset($num[$key])) {
        $index = $key;
    }
}
foreach ($num as $key => $value) {
    $reverseNum[] = $num[$index];
    $index--;
}
echo '<pre>';
print($reverseNum);
echo '</pre>';

//3. Дана строка
//let str = 'Hi I am ALex'
//развенуть строку в обратном направлении.

//while

$str = 'Hi I am ALex';
$i = 0;
$index = 0;
$reverseStr = '';
while (isset($str[$i])) {
    $index = $i;
    $i++;
}
while ($index >= 0) {
    $reverseStr .= $str[$index];
    $index--;
}
echo '<pre>';
print($reverseStr);
echo '</pre>';

//do while


$str = 'Hi I am ALex';
$i = 0;
$reverseStr = '';
while (isset($str[$i])) {
    $index = $i;
    $i++;
}
do {
    $reverseStr .= $str[$index];
    $index--;
} while ($index >= 0);
echo '<pre>';
print($reverseStr);
echo '</pre>';

//for

$str = 'Hi I am ALex';
$index = 0;
$reverseStr = '';
for ($i = 0; $i >= 0; $i++) {
    if (isset($str[$i])) {
        $index = $i;
    } else {
        break;
    }
}
for ($i = $index; $i >= 0; $i--) {
    $reverseStr .= $str[$i];
}
echo '<pre>';
print($reverseStr);
echo '</pre>';

//foreach

$str = 'Hi I am ALex';
$i = 0;
$index = 0;
$arrStr = [];
$reverseArr = [];
$reverseStr = '';
while (isset($str[$i])) {
    $arrStr[] = $str[$i];
    $i++;
}
foreach ($arrStr as $key => $value) {
    if (isset($arrStr[$key])) {
        $i = $key;
    }
}
foreach ($arrStr as $key => $value) {
    $reverseArr[] = $arrStr[$i];
    $index = $i;
    $i--;
}
foreach ($reverseArr as $value) {
    $reverseStr .= $reverseArr[$index];
    $index++;
}
echo '<pre>';
print($reverseStr);
echo '</pre>';

//4. Дана строка. готовую функцию toUpperCase() or tolowercase()
//let str = 'Hi I am ALex'
//сделать ее с с маленьких букв

//while

$str = 'Hi I am ALex';
$i = 0;
$lowerStr = '';
while (isset($str[$i])) {
    $lowerStr .= mb_strtolower($str[$i]);
    $i++;
}
echo '<pre>';
echo $lowerStr;
echo '</pre>';

//do while

$str = 'Hi I am ALex';
$i = 0;
$lowerStr = '';

do {
    $lowerStr .= mb_strtolower($str[$i]);
    $i++;
} while (isset($str[$i]));
echo '<pre>';
echo $lowerStr;
echo '</pre>';

//for

$str = 'Hi I am ALex';
$lowerStr = '';
for ($i = 0; $i >= 0; $i++) {
    if (isset($str[$i])) {
        $lowerStr .= mb_strtolower($str[$i]);
    } else {
        break;
    }

}
echo '<pre>';
echo $lowerStr;
echo '</pre>';


//foreach
$str = 'Hi I am ALex';
$i = 0;
$arrStr = [];
$lowerStr = '';
while (isset($str[$i])) {
    $arrStr[] = mb_strtolower($str[$i]);
    $i++;
}
$i = 0;
foreach ($arrStr as $value) {
    $lowerStr .= $arrStr[$i];
    $i++;
}
echo '<pre>';
echo $lowerStr;
echo '</pre>';

//5. Дана строка
//let str = 'Hi I am ALex'
//сделать все буквы большие

//while

$str = 'Hi I am ALex';
$i = 0;
$upperStr = '';
while (isset($str[$i])) {
    $upperStr .= mb_strtoupper($str[$i]);
    $i++;
}
echo '<pre>';
echo $upperStr;
echo '</pre>';

//do while

$str = 'Hi I am ALex';
$i = 0;
$upperStr = '';

do {
    $upperStr .= mb_strtoupper($str[$i]);
    $i++;
} while (isset($str[$i]));
echo '<pre>';
echo $upperStr;
echo '</pre>';

//for

$str = 'Hi I am ALex';
$upperStr = '';
for ($i = 0; $i >= 0; $i++) {
    if (isset($str[$i])) {
        $upperStr .= mb_strtoupper($str[$i]);
    } else {
        break;
    }

}
echo '<pre>';
echo $upperStr;
echo '</pre>';


//foreach
$str = 'Hi I am ALex';
$i = 0;
$arrStr = [];
$upperStr = '';
while (isset($str[$i])) {
    $arrStr[] = mb_strtoupper($str[$i]);
    $i++;
}
$i = 0;
foreach ($arrStr as $value) {
    $upperStr .= $arrStr[$i];
    $i++;
}
echo '<pre>';
echo $upperStr;
echo '</pre>';

//7. Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//сделать все буквы с маленькой

//while

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrLower = [];
$i = 0;
while (isset($arr[$i])) {
    $arrLower[] = mb_strtolower($arr[$i]);
    $i++;
}
echo '<pre>';
print($arrLower);
echo '<pre>';

//do while

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrLower = [];
$i = 0;
do {
    $arrLower[] = mb_strtolower($arr[$i]);
    $i++;
} while (isset($arr[$i]));
echo '<pre>';
print($arrLower);
echo '<pre>';

//for

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrLower = [];
for ($i = 0; isset($arr[$i]); $i++) {
    $arrLower[] = mb_strtolower($arr[$i]);
}
echo '<pre>';
print($arrLower);
echo '<pre>';

//foreach

$arr = ['Alex', 'Vasya', 'Tanya', 'Lena', 'Tolya'];
$arrLower = [];
$i = 0;
foreach ($arr as $value) {
    $arrLower[] = mb_strtolower($arr[$i]);
    $i++;
}
echo '<pre>';
print($arrLower);
echo '<pre>';

//8. Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//сделать все буквы с большой

//while

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrUpper = [];
$i = 0;
while (isset($arr[$i])) {
    $arrUpper[] = mb_strtoupper($arr[$i]);
    $i++;
}
echo '<pre>';
print($arrUpper);
echo '<pre>';

//do while

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrUpper = [];
$i = 0;
do {
    $arrUpper[] = mb_strtoupper($arr[$i]);
    $i++;
} while (isset($arr[$i]));
echo '<pre>';
print($arrUpper);
echo '<pre>';

//for

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrUpper = [];
for ($i = 0; isset($arr[$i]); $i++) {
    $arrUpper[] = mb_strtoupper($arr[$i]);
}
echo '<pre>';
print($arrUpper);
echo '<pre>';

//foreach

$arr = ['Alex', 'Vasya', 'Tanya', 'Lena', 'Tolya'];
$arrUpper = [];
$i = 0;
foreach ($arr as $value) {
    $arrUpper[] = mb_strtoupper($arr[$i]);
    $i++;
}
echo '<pre>';
print($arrUpper);
echo '<pre>';

//9. Дано число
//let num = 1234678
//развернуть ее в обратном направлении

//while

$num = 1234678;
$num = (string)$num;
$i = 0;
$index = 0;
$reverseNum = '';
while (isset($num[$i])) {
    $index = $i;
    $i++;
}
while ($index >= 0) {
    $reverseNum .= $num[$index];
    $index--;
}
$reverseNum = (integer)$reverseNum;
echo '<pre>';
echo $reverseNum;
echo '</pre>';

//do while

$num = 1234678;
$num = (string)$num;
$i = 0;
$index = 0;
$reverseNum = '';
while (isset($num[$i])) {
    $index = $i;
    $i++;
}
do {
    $reverseNum .= $num[$index];
    $index--;
} while ($index >= 0);
$reverseNum = (integer)$reverseNum;
echo '<pre>';
echo $reverseNum;
echo '</pre>';

//for

$num = 1234678;
$num = (string)$num;
$index = 0;
$reverseNum = '';
for ($i = 0; $i >= 0; $i++) {
    if (isset($num[$i])) {
        $index = $i;
    } else {
        break;
    }
}
for ($i = $index; $i >= 0; $i--) {
    $reverseNum .= $num[$i];
}
$reverseNum = (integer)$reverseNum;
echo '<pre>';
echo $reverseNum;
echo '</pre>';

//foreach

$num = 1234678;
$num = (string)$num;
$i = 0;
$index = 0;
$arrNum = [];
$reverseArr = [];
$reverseNum = '';
while (isset($num[$i])) {
    $arrNum[] = $num[$i];
    $i++;
}
foreach ($arrNum as $key => $value) {
    if (isset($arrNum[$key])) {
        $i = $key;
    }
}
foreach ($arrNum as $key => $value) {
    $reverseArr[] = $arrNum[$i];
    $index = $i;
    $i--;
}
foreach ($reverseArr as $value) {
    $reverseNum .= $reverseArr[$index];
    $index++;
}
$reverseNum = (integer)$reverseNum;
echo '<pre>';
echo $reverseNum;
echo '</pre>';

//10. Дан массив
//[44, 12, 11, 7, 1, 99, 43, 5, 69]
//отсортируй его в порядке убывания
//while
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
$i = 0;
while (isset($arr[$i])) {
    $count = $i + 1;
    $i++;
}
$i = 0;
$j = 0;
while ($i < $count) {
    while ($j < $count - 1) {
        if ($arr[$j] > $arr[$j + 1]) {
            $num = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $num;
        }
        $j++;
    }
    $i++;
    if ($j == $count - 1) {
        $j = 0;
    }
}
echo '<pre>';
print($arr);
echo '<pre>';

//do while

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
$i = 0;
do {
    $count = $i;
    $i++;
} while (isset($arr[$i]));
$i = 0;
$j = 0;
do {
    do {
        if ($arr[$j] > $arr[$j + 1]) {
            $num = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $num;
        }
        $j++;
    } while ($j < $count);
    $i++;
    if ($j == $count) {
        $j = 0; }

} while ($i < $count);

echo '<pre>';
print($arr);
echo '<pre>';

//for

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
for ($i = 0; isset($arr[$i]); $i++) {
    $count = $i + 1; }

for ($i = 0; $i < $count; $i++) {
    for ($j = 0; $j < $count - 1; $j++) {
        if ($arr[$j] > $arr[$j + 1]) {
            $num = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $num;
        }
    }
}
echo '<pre>';
print($arr);
echo '<pre>';

//foreach

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
for ($i = 0; isset($arr[$i]); $i++) {
    $count = $i;
}
foreach ($arr as $value) {
    for ($j = 0; $j < $count; $j++) {
        if ($arr[$j] > $arr[$j + 1]) {
            $num = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $num;
        }
    }
}
echo '<pre>';
print($arr);
echo '<pre>';
