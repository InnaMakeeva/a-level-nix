<?php
// phpcs:disable
echo '<p>Задача 1</p>';
// phpcs:enable
$a = 42;
$b = 55;
echo $a > $b ? $a . ' больше, чем $b' : $b . ' больше, чем $a';
echo '<br>';

// phpcs:disable
echo '<p>Задача 2</p>';
// phpcs:enable
$a = rand(5, 15);
$b = rand(5, 15);
echo $a > $b ? $a . ' больше, чем $b' : $b . ' больше, чем $a';
echo '<br>';

$a = rand(5, 15);
$b = rand(5, 15);
if ($a > $b) {
    echo $a . ' больше, чем ' . $b;
} elseif ($a < $b) {
    echo $a . ' меньше, чем ' $b;
} else {
    echo $a . ' равно '. $b};
echo '<br>';

// phpcs:disable
echo '<p>Задача 3</p>';
// phpcs:enable
$f = 'Makeeva';
$i = 'Inna';
$o = 'Nikolaevna';
echo $f . ' ' . $i[0] . '. ' . $o[0] . '.';
echo '<br>';

// phpcs:disable
echo '<p>Задача 4</p>';
// phpcs:enable
$a = rand(1, 99999);
$b = rand(0, 9);
echo 'В число ' . $a . ' цифра ' . $b . ' входит ' . substr_count($a, $b) . ' раз.';
echo '<br>';

// phpcs:disable
echo '<p>Задача 5</p>';
// phpcs:enable
$a = 3;
echo 'a = ' . $a;
echo '<br>';

$a = 10;
$b = 2;
echo 'Сумма чисел ' . $a . ' и ' . $b .  ' равна ' . ($a + $b);
echo '<br>';
echo 'Разность чисел ' . $a . ' и ' . $b .  ' равна ' . ($a - $b);
echo '<br>';
echo 'Произведение чисел ' . $a . ' и ' . $b .  ' равно ' . ($a * $b);
echo '<br>';
echo 'Частное чисел ' . $a . ' и ' . $b .  ' равно ' . ($a / $b);
echo '<br>';

$c = 15;
$d = 2;
$result = $c + $d;
echo $result;
echo '<br>';

$a = 10;
$b = 2;
$c = 5;
echo $a + $b + $c;
echo '<br>';

$a = 17;
$b = 10;
$c = $a - $b;
$d = $c;
echo 'd = ' . $d;
echo '<br>';

// phpcs:disable
echo '<p>Задача 6</p>';
// phpcs:enable
$c = rand(1, 999);
$d = rand(1, 999);
$result = $c + $d;
echo $result;
echo '<br>';

// phpcs:disable
echo '<p>Задача 7</p>';
// phpcs:enable
$text = 'Привет, Мир!';
echo $text;
echo '<br>';

$text1 = 'Привет, ';
$text2 = 'Мир!';
echo $text1 . $text2;
echo '<br>';

$hour = 60 * 60;
$day = $hour * 24;
$week = $day * 7;
$month = $day * 30;
echo 'В часе ' . $hour . ' секунд, в сутках ' . $day . ' секунд, в неделе ' . $week .
    ' секунд, в месяце ' . $month . ' секунд.';
echo '<br>';

// phpcs:disable
echo '<p>Задача 8</p>';
// phpcs:enable
$var = 1;
$var += 12;
$var -= 14;
$var *= 5;
$var /= 7;
++$var;
--$var;
echo $var;
// phpcs:disable
echo '<p>Задача 9</p>';
$hour = date('H');
$min = date('i');
$sec = date('s');
echo 'Текущее время: ' . $hour . ':' . $min . ':' . $sec;
echo '<br>';

// phpcs:disable
echo '<p>Задача 10</p>';
// phpcs:enable
$text = 'Я';
$text .= ' хочу';
$text .= ' знать';
$text .= ' PHP!';
echo $text;
echo '<br>';

// phpcs:disable
echo '<p>Задача 11</p>';
// phpcs:enable
$foo = 'bar';
$bar = 10;
echo $$foo;
echo '<br>';

// phpcs:disable
echo '<p>Задача 12</p>';
// phpcs:enable
$a = 2;
$b = 4;
echo ($a++) + $b; // а = 2, b = 4
echo '<br>';
echo $a + (++$b); // a = 3, b = 5
echo '<br>';
echo (++$a) + $b++; // a = 4, b = 5
echo '<br>';

// phpcs:disable
echo '<p>Задача 13</p>';
// phpcs:enable
$a = 'php';
echo isset($a) ? 'переменная существует' : 'переменная не существует';
echo '<br>';
echo gettype($a) == 'string' ? 'переменная строка ' : 'переменная не строка';
echo '<br>';
echo is_null($a) ? 'NULL' : 'не NULL';
echo '<br>';
echo empty($a) ? 'переменная пуста' : 'переменная не пуста, в ней ' . gettype($a);
echo '<br>';
echo is_integer($a) ? 'переменная integer' : 'переменная не integer, она ' . gettype($a);
echo '<br>';
echo is_double($a) ? 'переменная double' : 'переменная не double, она ' . gettype($a);
echo '<br>';
echo is_string($a) ? 'переменная строка' : 'переменная не строка';
echo '<br>';
echo is_numeric($a) ? 'переменная число' : 'переменная не число';
echo '<br>';
echo is_bool($a) ? 'переменная bool' : 'переменная не bool';
echo '<br>';
echo is_scalar($a) ? 'скалярная переменная ' : 'не скалярная переменная ';
echo '<br>';
echo is_null($a) ? 'значение переменной NULL ' : 'значение переменной не NULL ';
echo '<br>';
echo is_array($a) ? 'переменная массив' : 'переменная не массив';
echo '<br>';
echo is_object($a) ? 'переменная обьект' : 'переменная не обьект';
echo '<br>';

// phpcs:disable
echo '<p>Задача 14</p>';
// phpcs:enable
$c = rand(1, 999);
$d = rand(1, 999);
$sum = $c + $d;
$prod = $c * $d;
echo 'Сумма: ' . $sum . '. Произведение: ' . $prod;
echo '<br>';

// phpcs:disable
echo '<p>Задача 15</p>';
// phpcs:enable
$c = rand(1, 10);
$d = rand(1, 10);
$sum = $c ** 2 + $d ** 2;
echo 'Сумма квадратов: ' . $sum;
echo '<br>';

// phpcs:disable
echo '<p>Задача 16</p>';
// phpcs:enable
$c = rand(1, 99);
$d = rand(1, 99);
$e = rand(1, 99);
$mean = ($c + $d + $e) / 3;
echo 'Среднее арифметическое: ' . $mean;
echo '<br>';

// phpcs:disable
echo '<p>Задача 17</p>';
// phpcs:enable
$x = rand(1, 9);
$y = rand(1, 9);
$z = rand(1, 9);
$result = ($x + 1) - 2 * ($z - (2 * $x) + $y);
echo 'Выражение равно: ' . $result;
echo '<br>';

// phpcs:disable
echo '<p>Задача 18</p>';
// phpcs:enable
$x = rand(1, 50);
$res1 = $x % 3;
$res2 = $x % 5;
$res3 = ($x * 30) / 100;
$res4 = ($x * 120) / 100;
echo 'Остаток1: ' . $res1 . ', остаток2: ' . $res2 . ', 30% - это ' . $res3 . ', 120% - это ' . $res4;
echo '<br>';

// phpcs:disable
echo '<p>Задача 19</p>';
// phpcs:enable
$c = rand(1, 99);
$d = rand(1, 99);
$res1 = ($c * 40) / 100;
$res2 = ($d * 84) / 100;
$a = 567;
$a = (string)$a;
$sum = $a[0] + $a[1] + $a[2];
echo '40% - это ' . $res1 . ', 84% - это ' . $res2 . ', сумма: ' . $sum;
echo '<br>';

// phpcs:disable
echo '<p>Задача 20</p>';
// phpcs:enable
$a = 123;
$a = (string)$a;
$a[1] = '0';
$a = $a[2] . $a[1] . $a[0];
echo $a;
echo '<br>';

// phpcs:disable
echo '<p>Задача 21</p>';
// phpcs:enable
$x = rand(1, 50);
echo ($x % 2 == 0) ? 'Число ' . $x . ' четное' : 'Число ' . $x . ' нечетное';
echo '<br>';
